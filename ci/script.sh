set -ex

main() {
    sh build-docker-image.sh $TARGET

    if [ $TRAVIS_BRANCH = master ]; then
        return
    fi

    local examples=(
        _llseek
        args
        chdir
        create
        dup
        env
        format
        hashmap
        hello
        instant
        ls
        open
        preadwrite
        stat
        stderr
        system-time
        tcp_listen_connect
        thread
        vec
        zero
    )

    for example in ${examples[@]}; do
        cross run \
              --target $TARGET \
              --no-default-features \
              --example $example
    done

    for example in ${examples[@]}; do
        cross run \
              --target $TARGET \
              --no-default-features \
              --example $example --release
    done

    cat >>Xargo.toml <<'EOF'

[dependencies.std]
default-features = false
path = "/project"
stage = 2

[dependencies.test]
path = "/project/test"
stage = 3
EOF

    cross test \
          --target $TARGET \
          --no-default-features

    set +x
    pushd target/$TARGET/release/examples
    size ${examples[@]}
    popd
}

main
